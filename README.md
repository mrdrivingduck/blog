# blog

🦆 Dynamic blog system based on [Vue.js](https://vuejs.org/).

Created by : Mr Dk.

2019 / 07 / 01 15:44

@Nanjing, Jiangsu, China

---

## Build Status

| Environment | Status                                                       |
| ----------- | ------------------------------------------------------------ |
| Development | ![Build Only](https://github.com/mrdrivingduck/blog/workflows/Build%20Only/badge.svg) |
| Production  | ![Build and Deploy](https://github.com/mrdrivingduck/blog/workflows/Build%20and%20Deploy/badge.svg?branch=master) |

## About

A dynamic blog system implemented by myself. Supported by:

* [Vue.js](https://vuejs.org/) - 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [Vuex](https://vuex.vuejs.org/) - 🗃️ Centralized State Management for Vue.js.
* [Vue Router](https://router.vuejs.org/) - 🚦 The official router for Vue.js.
* [Vue CLI](https://cli.vuejs.org/) - 🛠️ Standard Tooling for Vue.js Development.
* [Vue Clipboard 2](https://github.com/Inndy/vue-clipboard2) - A simple Vue 2 binding to clipboard.js.
* [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js.
* [Element](https://element.eleme.io/) - A Vue.js 2.0 UI Toolkit for Web.
* [Marked.js](https://marked.js.org/) - A markdown parser and compiler. Built for speed.
* [Highlight.js](https://github.com/highlightjs/highlight.js) - JavaScript syntax highlighter.
* [GitHub Markdown CSS Dark](https://github.com/mrdrivingduck/github-markdown-css) - The minimal amount of CSS to replicate the GitHub Markdown style.
* [GitHub API v4](https://developer.github.com/v4/) - The official GitHub GraphQL API v4.
* [GitHub Pages](https://pages.github.com/) - Websites for you and your projects.
* [GitHub Actions](https://github.com/features/actions) - Automate your workflow from idea to production.
* [GitHub Buttons](https://buttons.github.io/) - Unofficial github:button component for Vue.js.
* [Aliyun](https://www.aliyun.com/) - More Than Just Cloud.

---

## Project setup

### Install dependencies

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```

### Compiles and minifies for production

```bash
npm run build
```

### Run your tests

```bash
npm run test
```

### Lints and fixes files

```bash
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

---

## License

Copyright © 2018-2021, Jingtang Zhang. ([MIT License](LICENSE))

---

