# Icon Source

From https://www.iconfont.cn/

* light - `#2c2c2c`
* dark - `#ffffff`

`16 × 16` - `.svg` file

---

